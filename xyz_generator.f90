!program to generate initial positions of molecule

MODULE consts
real::k=1
real::a=1
real::b=1.5
real::a_sc=1.2
END MODULE consts

program md
use consts
IMPLICIT NONE
INTEGER::n,y1,i
CHARACTER(len=5)::ele
REAL, allocatable::xa(:),ya(:),za(:)
INTEGER, allocatable::x(:),y(:),z(:)

open(1,file="read1.txt")

write(*,*)'ENTER <ELEMENT_NAME> <SPACE> <NO_OF_ATOMS>'
read(*,*)ele,n
allocate (x(n),y(n),z(n),xa(n),ya(n),za(n))
write(*,*)'SPECIFY HOW ATOMS ARE ARRANGED. PRESS 1 FOR RANDOM OR PRESS 2 FOR SIMPLE CUBIC ARRANGEMENT'
read(*,*)y1

IF(y1==1)then
call xyz_rand(n,xa,ya,za)
ELSE
call xyz_sc(n,x,y,z)
END IF

WRITE(1,*)n
WRITE(1,*)
DO i=1,n

IF(y1==1)then
WRITE(1,*)ele,xa(i),ya(i),za(i)
ELSE
WRITE(1,*)ele,x(i),y(i),z(i)
!WRITE(1,*)ele,a_sc*x(i),a_sc*y(i),a_sc*z(i)
END IF
END DO

deallocate (x,y,z,xa,ya,za)

close(1)
end program md

subroutine xyz_rand(n,x,y,z)
use consts
IMPLICIT NONE
INTEGER::n,i=1,j,f1=1,f2=0
REAL::rand,x1,y1,z1,d,x(n),y(n),z(n)

DO 

k=k+1e-3
x1=k*rand(0)
y1=k*rand(0)
z1=k*rand(0)

f1=1;f2=0
DO j=1,i-1
d=sqrt((x(j)-x1)**2 + (y(j)-y1)**2 + (z(j)-z1)**2)

IF (d .le. a)then
f1=0
END IF

IF (d .le. b .and. f2==0)then
f2=1
END IF
END DO

IF ((f1==1 .and. f2==1) .or. i==1)then
x(i)=x1
y(i)=y1
z(i)=z1
i=i+1
END IF

IF (i==n+1)then
EXIT
END IF

END DO

end subroutine xyz_rand

subroutine xyz_sc(n,x,y,z)
use consts
IMPLICIT NONE
INTEGER::n,i,j,t=2,inc,m,l=1,x(n),y(n),z(n),aa,bb,cc,p,q,r,xp,yp,zp,f=0

x(1)=0
y(1)=0
z(1)=0

aa=x(1)
bb=y(1)
cc=z(1)

DO
DO i=1,3
inc=1
DO j=1,2

IF (i==1)then
p=inc;q=0;r=0
ELSE IF (i==2)then
p=0;q=inc;r=0
ELSE
p=0;q=0;r=inc
END IF
inc=-1

xp=x(l)+p
yp=y(l)+q
zp=z(l)+r
write(*,*)i,j,l
f=0
DO m=1,t-1
IF(xp==x(i) .and. yp==y(i) .and. zp==z(i))then
f=1
END IF
END DO

IF (f==0)then
x(t)=xp
y(t)=yp
z(t)=zp
t=t+1
END IF

IF(t==n+1)then
EXIT
END IF

END DO

IF(t==n+1)then
EXIT
END IF

END DO

aa=x(l+1)
bb=y(l+1)
cc=z(l+1)
l=l+1

IF(t==n+1)then
EXIT
END IF
END DO

end subroutine xyz_sc