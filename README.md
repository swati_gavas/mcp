# MCP

These are some selected codes in Fortran from MCP (Methods of Computational Physics) course which I had undertaken during my masters

1) kep_3b_rk : This is the code for solving Kepler's three body problem using Runge-Kuta method.
	       output files:- 3b_kep.dat - outputs the positions of all the masses with respect to time 

2) len_J : This is the code for obtaining ground state of N number of atoms/molecules which interacts with each other under Lennard-Jones potential.

3) schrodinger_eq_nmrv : This is the code for solving schrodinger equation for given potential well using Numerov's method.
	                 output files:- psi_n.dat - Wavefunctoin of nth state
		                        prob_den.dat - probability density of nth state	


4) xyz_generator : This is the code to generate initial positions of atoms/molecules suitable for molecular dynamics codes e.g. len_J