!three body kepler problem - rk method


program kep
IMPLICIT NONE
INTEGER::i
REAL::t,h,x1,x2,x3,y1,y2,y3,vx1,vx2,vx3,vy1,vy2,vy3,m1,m2,m3,r12,r13,r23,x1n
REAL::x2n,x3n,y1n,y2n,y3n,v2n,v3n,pi=4*atan(1.),vx1n,vx2n,vx3n,vy1n,vy2n,vy3n

!problem specifications-----------
t=0
h=0.01

m1=1
x1=0
y1=0
vx1=0
vy1=0

m2=3e-6
x2=1
y2=0
vx2=0
vy2=sqrt(4*pi**2*m1)

m3=0.000945
x3=5
y3=0
vx3=0
vy3=sqrt(((4*pi**2*m1)/25+(4*pi**2*m2)/16)*5)
!---------------------------------

open(5,file="3b_kep.dat")
WRITE(5,*) "Time	","x_mass1	","y_mass1	","x_mass2	","y_mass2	","x_mass3	","y_mass3"
DO i=1,1200
 WRITE(5,*)t,x1,y1,x2,y2,x3,y3

 r12=sqrt((x1-x2)**2+(y1-y2)**2)	!=r21
 r13=sqrt((x1-x3)**2+(y1-y3)**2)	!=r31
 r23=sqrt((x2-x3)**2+(y2-y3)**2)	!=r32


 call rk(h,m2,m3,r12,r13,x1,x2,x3,vx1,x1n,vx1n)
 call rk(h,m2,m3,r12,r13,y1,y2,y3,vy1,y1n,vy1n)

 call rk(h,m3,m1,r23,r12,x2,x3,x1,vx2,x2n,vx2n)
 call rk(h,m3,m1,r23,r12,y2,y3,y1,vy2,y2n,vy2n)

 call rk(h,m1,m2,r13,r23,x3,x1,x2,vx3,x3n,vx3n)
 call rk(h,m1,m2,r13,r23,y3,y1,y2,vy3,y3n,vy3n)

  x1=x1n
  y1=y1n
  vx1=vx1n
  vy1=vy1n

  x2=x2n
  y2=y2n
  vx2=vx2n
  vy2=vy2n

  x3=x3n
  y3=y3n
  vx3=vx3n
  vy3=vy3n
  t=t+h

END DO
END PROGRAM kep



!subroutine to calculate position and velocity---------------------------

SUBROUTINE rk(h,mp,mq,rp,rq,z,zp,zq,vz,zn,vzn)
IMPLICIT NONE
REAL::h,z,zp,zq,vz,zn,vzn,kz1,kz2,kz3,kz4,kvz1,kvz2,kvz3,kvz4,fz,fvz,pi=4*atan(1.),mp,mq,rp,rq,p,q

p=4*pi**2*mp/rp**3
q=4*pi**2*mq/rq**3

kz1=fz(vz)*h
kvz1=fvz(z,zp,zq,p,q)*h

kz2=fz(vz+kvz1/2)*h
kvz2=fvz(z+kz1/2,zp,zq,p,q)*h

kz3=fz(vz+kvz2/2)*h
kvz3=fvz(z+kz2/2,zp,zq,p,q)*h

kz4=fz(vz+kvz3)*h
kvz4=fvz(z+kz3,zp,zq,p,q)*h

zn=z+(kz1+2*kz2+2*kz3+kz4)/6
vzn=vz+(kvz1+2*kvz2+2*kvz3+kvz4)/6

return
END SUBROUTINE rk

!Functions-----------------------------------------------------------

FUNCTION fz(vz)
REAL::fz,vz
fz=vz
end FUNCTION fz

FUNCTION fvz(z1,z2,z3,p,q)
REAL::fvz,z,k
fvz=p*(z2-z1)+q*(z3-z1)
end FUNCTION fvz