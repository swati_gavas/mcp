MODULE consts
real::epsi4=1
real::sigm=1
real::m=1
real::h=0.01
real::Kb=1!boltzmann const
integer::itr=70000
END MODULE consts

program md
use consts
IMPLICIT NONE
INTEGER::n,i,k=1
REAL::ke,pe,te,T,fxo,fyo,fzo,T0,alp,T1,beta
 CHARACTER(len=2), allocatable::atom(:)
REAL, allocatable::x(:),y(:),z(:),vx(:),vy(:),vz(:),vxo(:),vyo(:),vzo(:),d(:,:),xo(:),yo(:),zo(:),fx(:),fy(:),fz(:)

open(1,file="read.txt")
open(2,file="write.xyz")
open(3,file="energy.txt")

read(1,*)n
read(1,*)

allocate (atom(n),x(n),y(n),z(n),vx(n),vy(n),vz(n),vxo(n),vyo(n),vzo(n),d(n,n),fx(n),fy(n),fz(n),xo(n),yo(n),zo(n))

vx=0;vy=0;vz=0;

DO i=1,n
read(1,*)atom(i),xo(i),yo(i),zo(i)
END DO

DO k=2,itr
write(2,*)n,k
write(2,*)
DO i=1,n
call dist(xo,yo,zo,n,d)
call energy(n,d,vxo,vyo,vzo,ke,pe,te,T)
WRITE(2,*)atom(i),xo(i),yo(i),zo(i)!,vxo(i),vyo(i),vzo(i)
WRITE(3,*)k-1,te,ke,pe,T
END DO

DO i=1,n
call dist(xo,yo,zo,n,d)
call force(xo,n,d,fx)
call force(yo,n,d,fy)
call force(zo,n,d,fz)

x(i)=xo(i)+h*vx(i)+(h**2)*fx(i)/(2*m)
y(i)=yo(i)+h*vy(i)+(h**2)*fy(i)/(2*m)
z(i)=zo(i)+h*vz(i)+(h**2)*fz(i)/(2*m)

IF (k.le.2000)then
fxo=fx(i)
fyo=fy(i)
fzo=fz(i)
call dist(x,y,z,n,d)
call force(x,n,d,fx)
call force(y,n,d,fy)
call force(z,n,d,fz)

vx(i)=vxo(i)+(h/2/m)*(fxo+fx(i))
vy(i)=vyo(i)+(h/2/m)*(fyo+fy(i))
vz(i)=vzo(i)+(h/2/m)*(fzo+fz(i))

IF (k==2000)then
call energy(n,d,vx,vy,vz,ke,pe,te,T0)
alp=(1-T0)/10000
write(*,*)T0
END IF

ELSE IF(k.le.12000)then
vx(i)=sqrt(Kb*(T0+alp*(k-2000))/m/n)
vy(i)=sqrt(Kb*(T0+alp*(k-2000))/m/n)
vz(i)=sqrt(Kb*(T0+alp*(k-2000))/m/n)

IF (k==12000)then
call energy(n,d,vx,vy,vz,ke,pe,te,T1)
write(*,*)T1
END IF

ELSE IF(k.le.30000)then
!~ fxo=fx(i)
!~ fyo=fy(i)
!~ fzo=fz(i)
!~ call dist(x,y,z,n,d)
!~ call force(x,n,d,fx)
!~ call force(y,n,d,fy)
!~ call force(z,n,d,fz)

!~ vx(i)=vxo(i)+(h/2/m)*(fxo+fx(i))
!~ vy(i)=vyo(i)+(h/2/m)*(fyo+fy(i))
!~ vz(i)=vzo(i)+(h/2/m)*(fzo+fz(i))

!~ IF (mod(k,100)==0)then
vx(i)=sqrt(Kb*(T1)/m/n)
vy(i)=sqrt(Kb*(T1)/m/n)
vz(i)=sqrt(Kb*(T1)/m/n)
!~ END IF

beta=T1/40000
ELSE IF(k.le.70000)then
vx(i)=sqrt(Kb*(T1-beta*(k-30000))/m/n)
vy(i)=sqrt(Kb*(T1-beta*(k-30000))/m/n)
vz(i)=sqrt(Kb*(T1-beta*(k-30000))/m/n)

END IF

END DO
xo=x
yo=y
zo=z
vxo=vx
vyo=vy
vzo=vz
END DO

end program md

SUBROUTINE dist(x,y,z,n,d)
use consts
integer::i,j,n
real::x(n),y(n),z(n),d(n,n)

d=0
DO i=1,n
DO j=i+1,n
d(i,j)=sqrt((x(i)-x(j))**2 + (y(i)-y(j))**2 + (z(i)-z(j))**2)
d(j,i)=d(i,j)
END DO
END DO
END SUBROUTINE dist

SUBROUTINE force(r,n,d,f)
use consts
integer::i,j,n
real::r(n),d(n,n),frc(n,n),f(n),sum_fi

frc(n,n)=0
DO i=1,n
sum_fi=0
DO j=i+1,n
IF(j.ne.i)then
frc(i,j)=(6*epsi4/d(i,j)**2)*(2*(sigm/d(i,j))**12 -(sigm/d(i,j))**6)*(r(i)-r(j))
frc(j,i)=-frc(i,j)
END IF
sum_fi=sum_fi+frc(i,j)
END DO
f(i)=sum_fi
END DO
END SUBROUTINE force

SUBROUTINE energy(n,d,vx,vy,vz,ke,pe,te,T)
use consts
integer::i,j,n
real::ke,pe,vx(n),vy(n),vz(n),d(n,n),T

ke=0
pe=0
DO i=1,n
ke=ke+0.5*(vx(i)**2 + vy(i)**2 + vz(i)**2)
DO j=1,n
IF(i .ne. j)then
pe=pe + epsi4*((sigm/d(i,j))**12 - (sigm/d(i,j))**6)
END IF 
END DO
END DO
pe=0.5*pe
te=pe+ke
T=2*ke/3/Kb
END SUBROUTINE energy