!solution to schordinger equation using Numerov method
!2m/h^2=0.4829 Mev^-1 fm^-2
!m=940 MeV

MODULE initial_cond
IMPLICIT NONE
!problem specifications-------------------------------------------------------------------------------------------
real::a=2.			!fm (e-15)
real::h=0.005
real::E0=-85.			!Mev
real::V=-86.			!MeV
END MODULE initial_cond


program qm	!--------------------------------------------------------------------------------------------------
use initial_cond
IMPLICIT NONE
Integer::j,n=1,i,p,s
REAL::E,E1,E2,prod,d(900),x(10000,2),psi(10000,2),Ee(10),egn_psi(10000,2)
REAL::l,m,Norm,sum_e=0,sum_o=0,N0

!Files--------------------------------------------------
open(1,file="sdngr_nmrv.dat")
open(2,file="sdngr_nmrv_ld.dat")
open(3,file="psi_n.dat")
open(4,file="prob_den.dat")

E=E0
DO j=1,850

	call dld(E,x,psi,d(j))

	IF(j.ge.2)then
	prod=d(j)*d(j-1)	
	END IF 

	write(2,*)E,d(j)
	IF(prod<0 .and. j.gt.2 .and. d(j)<0)then
	E1=E
	E2=E-0.1
	call e_eigen(E1,E2,Ee(n))
	write(*,*)n-1,'th Energy eigevvalue = ',Ee(n) !n=quantum number
	n=n+1
	END IF
	E=E+0.1
END DO

write(*,*)'Which eigenstate you want to see? (n=0,1,2,3...)'
read(*,*)s
!Eigen-function----------------------------------------------
 call dld(Ee(s+1),x,psi,d(j))
p=6/h+1
!p=8/h+1
DO i=1,p
IF(mod(s,2)==0)then
WRITE(1,*)x(i,1),psi(i,1)
ELSE
WRITE(1,*)x(i,1),psi(i,1)
END IF
END DO

DO i=p,1,-1
IF(mod(s,2)==0)then
WRITE(1,*)x(i,2),psi(i,2)
ELSE
WRITE(1,*)x(i,2),-psi(i,2)
END IF
END DO

 close(1)
open(1,file="sdngr_nmrv.dat")
!Normalisation---------------------------------------------------
DO i=1,2402
READ(1,*)l,m
egn_psi(i,1)=l
egn_psi(i,2)=m
END DO

N0=egn_psi(1,2)**2+egn_psi(2402,2)**2
DO i=2,2401
IF(mod(i,2)==0)then
sum_e=sum_e+egn_psi(i,2)**2
else
sum_o=sum_o+egn_psi(i,2)**2
END IF
END DO
N0=(h/3)*(N0+2*sum_e+4*sum_o)
Norm=sqrt(N0)

DO i=1,2402
egn_psi(i,2)=egn_psi(i,2)/Norm
WRITE(3,*)egn_psi(i,1),egn_psi(i,2)
WRITE(4,*)egn_psi(i,1),egn_psi(i,2)**2
END DO

end program qm

!subroutine to get log_derevative difference value----------------------------------------------------
SUBROUTINE dld(E,x,psi,d)
use initial_cond
IMPLICIT NONE
INTEGER::i,q,p
REAL::k,l,x(10000,2),psi(10000,2),E,k2,d,LD_l,LD_r

p=8/h+3
q=p-1-4/h

!initial conditions-----------------------------------------------
x(1,1)=-6		!x(i,1) for left hand wave function
x(2,1)=-5.999		!x(i,2) for right hand wave function
psi(1,1)=0		!similar for psi(i,j)
psi(2,1)=1e-5

x(1,2)=6
x(2,2)=5.999
psi(1,2)=0
psi(2,2)=1e-5


!----------------------------------------------------------------
DO i=3,p

x(i,1)  = x(i-1,1)+h
x(i,2)  = x(i-1,2)-h
k	=(1+(1/12.)*(h**2)*k2(x(i,1),E))
l	=(1+(h**2)*(1/12.)*k2(x(i,2),E))

psi(i,1)=(2*(1-(5/12.)*(h**2)*k2(x(i-1,1),E))*psi(i-1,1) - (1+(1/12.)*(h**2)*k2(x(i-2,1),E))*psi(i-2,1))/k
psi(i,2)=(2*(1-(5/12.)*(h**2)*k2(x(i-1,2),E))*psi(i-1,2) - (1+(h**2)*(1/12.)*k2(x(i-2,2),E))*psi(i-2,2))/l

END DO

	LD_l=(psi(p,1)-psi(p-1,1))/(h*psi(p,1))
	LD_r=(-psi(q,2) +psi(q-1,2))/(h*psi(q,2))
	d=abs(LD_r)-abs(LD_l)

END SUBROUTINE dld


!subroutine to get energy eigenvalue----------------------------------------------------
SUBROUTINE e_eigen(E1,E2,E)
use initial_cond
IMPLICIT NONE
INTEGER::i
REAL::E1,E2,Em,E,x(10000,2),psi(10000,2),d1,d2,dm

DO i=1,100
Em=(E1+E2)/2
 call dld(E1,x,psi,d1)
 call dld(E2,x,psi,d2)
 call dld(Em,x,psi,dm)

IF (d1*dm<0)then
E2=Em
ELSE
E1=Em
END IF

IF ((E1-E2).le.0.0001)then
E=(E1+E2)/2
EXIT
END IF
END DO
END SUBROUTINE e_eigen


!function of eqation constant----------------------------------------------------------------------------------
FUNCTION k2(x,E)
use initial_cond
REAL::k2,E
IF(abs(x).le.a)then
k2=0.4829*(E-V)
ELSE
k2=0.4829*E
END IF
end FUNCTION k2